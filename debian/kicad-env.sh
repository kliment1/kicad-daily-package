# Env var for kicad github plugin
export KIGITHUB="https://github.com/KiCad"
export KICAD6_SYMBOL_DIR="/usr/share/kicad/library/"
export KICAD6_3DMODEL_DIR="/usr/share/kicad/3dmodels/"
export KICAD6_FOOTPRINT_DIR="/usr/share/kicad/modules/"
export KICAD6_TEMPLATE_DIR="/usr/share/kicad/template/"
# Env var for kicad pcbnew plugins
export KICAD_PATH=/usr/share/kicad
